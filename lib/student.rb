class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name, @last_name = first_name, last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    raise "error!" if has_conflict?(new_course)
    self.courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    load = Hash.new(0)
    self.courses.each {|course| load[course.department] += course.credits}
    load
  end

  def has_conflict?(new_course)
    courses.any? {|course| course.conflicts_with?(new_course)}
  end

end
